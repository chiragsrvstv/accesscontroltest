'use strict'

/*
|--------------------------------------------------------------------------
| SellerSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class SellerSeeder {
  async run () {
    const seller = await Factory.model("App/Models/Seller").createMany(5);
  }
}

module.exports = SellerSeeder
