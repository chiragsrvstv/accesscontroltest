"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class SellerSchema extends Schema {
  up() {
    this.create("sellers", (table) => {
      table.increments();
      table.string("sellername", 40).notNullable().unique();
      table.string("email", 20).notNullable().unique();
      table.string("password", 60).notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop("sellers");
  }
}

module.exports = SellerSchema;
