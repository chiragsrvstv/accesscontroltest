"use strict"

const Hash = use("Hash")
const User = use("App/Models/User")

const {newEnforcer} = require('casbin')

class UserController {
  async show({ request, response }) {
    const e = await newEnforcer('App/casbin_conf/model.conf', 'App/casbin_conf/policy.csv')

    const { username, password } = request.body
    
    // test object for ABAC
    const testResource = {
        owner: 'lujci'
    }
    
    // find user, send 404 if not found
    const user = await User.findBy({ username })
    if (!user) {
      return response.status(404).send()
    }

    // send 404 if password don't match
    if (!(await Hash.verify(password, user.password))) {
      return response.status(404).send()
    }

    // enforcing policies and check for access 
    if(!(await e.enforce(username, "product", "view"))) {
        return response.status(403).send("forbidden")    
    }

    return response.status(200).send(200)
  }
}

module.exports = UserController
